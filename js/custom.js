$(document).ready(function(){
  var map = L.map('map').setView([46.958, 20], 8);
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoiYmFsYWdhbjczIiwiYSI6ImNrMG5sc2M5bDAxNHUzbXFyNDQ4OHE4cmMifQ.Oe0Pbaos2GMWmCljO8uUoQ'
  }).addTo(map);

  // var calls stores the requests initiated in the last minute.
  // Developer limit is 10 requests per minutes and 500 requests per day.
  // Calls are measured according to Eastern Standard Time, that is UTC-5
  var calls = new Array();
  var totalcalls = 0;
  var totalcallsLimit = 500;
  var callsLimit = 10;
  var account = "developer";

  function setAccount() {
    account = document.getElementById('account-select').value;
    switch (account) {
      case 'drizzle': {
        totalcallsLimit = 5000;
        callsLimit = 100;
      }
      case 'shower': {
        totalcallsLimit = 100000;
        callsLimit = 1000;
      }
      default: {
        totalcallsLimit = 500;
        callsLimit = 10;
      }
    }
  }
  setAccount();
  document.getElementById('account-select').addEventListener('change', setAccount);
  var dateObject = new Date();
  var date = dateObject.getFullYear().toString() + ("0" + (dateObject.getMonth() + 1).toString()).slice(-2) + ("0" + dateObject.getDate().toString()).slice(-2);

  document.getElementById('date-input').value = date;
  document.getElementById('date-input').addEventListener('input', setDate);
  function setDate() {
    date = document.getElementById('date-input').value;
  }

  var apiKey = document.getElementById('api-key').value;;
  document.getElementById('api-key').addEventListener('input', setApiKey);
  function setApiKey() {
    apiKey = document.getElementById('api-key').value;
  }


  function rightClickOnMap(event) {
    var lat = event.latlng.lat
    var lng = event.latlng.lng;
    arrayClean(calls);
    if (typeof apiKey != 'undefined' && apiKey.length > 0) {
      if (totalcalls < totalcallsLimit) {
        if (calls.length < callsLimit) {
          $.ajax({
            url : "http://api.wunderground.com/api/" + apiKey + "/geolookup/q/" + lat + "," + lng + ".json",
            dataType : "jsonp",
            success : function(parsed_json) {
              increaseCalls();
              var stations = parsed_json['location']['nearby_weather_stations'];
              // *** Loop through airports 
              for (i=0; i < stations['airport']['station'].length; i++) {
                station = stations['airport']['station'][i];

                // Only display aiports if they have icao.
                if (station['icao'].length > 0) {
                  var airportIcon = L.icon({
                    iconUrl: './images/airport.png',
                    // shadowUrl: 'leaf-shadow.png',
                  
                    // iconSize:     [38, 95], // size of the icon
                    //shadowSize:   [50, 64], // size of the shadow
                    //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
                    //shadowAnchor: [4, 62],  // the same for the shadow
                    //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                  });
                  L.marker([station['lat'], station['lon']], {icon: airportIcon}).addTo(map);
                  /*
                  var myLatlng = new google.maps.LatLng(station['lat'],station['lon']);
                  var marker = new google.maps.Marker({
                    position: myLatlng,
                    title: station['city'],
                    icon: './images/airport.png',
                    info: station['icao'],
                    map: map,
                  });
                  */
                 /*
                  google.maps.event.addListener(marker, 'click', function () {
                    var airport = this.info;
                    var url = 'https://www.wunderground.com/history/airport/' + airport;
                    var content = '<a href="' + url + '">' + url + '</a>';
                    content += '<div id="svg-div"></div>';
                    arrayClean(calls);
                    getAirportHistory(processAirportHistory, airport, date, this);
                    infowindow.setContent(content);
                    infowindow.open(map, this);
                  });
                  */
                }
              }
              // *** Loop through personal weather stations
              for (i=0; i < stations['pws']['station'].length; i++) {
                station = stations['pws']['station'][i];
                var pwsIcon = L.icon({
                  iconUrl: './images/pws.png',
                  // shadowUrl: 'leaf-shadow.png',
                
                  // iconSize:     [38, 95], // size of the icon
                  //shadowSize:   [50, 64], // size of the shadow
                  //iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
                  //shadowAnchor: [4, 62],  // the same for the shadow
                  //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                });
                L.marker([station['lat'], station['lon']], {icon: pwsIcon}).addTo(map);
                /*
                var myLatlng = new google.maps.LatLng(station['lat'],station['lon']);
                var marker = new google.maps.Marker({
                  position: myLatlng,
                  title: station['id'] + ' ' + station['neighborhood'],
                  icon: './images/pws.png',
                  id: station['id'],
                  info: '<p>id: ' + station['id'] + '<BR>City: ' + station['city'] + '<BR>Neighborhood: ' + station['neighborhood'] + '</p>',
                  map: map,
                });
                */
                /*
                google.maps.event.addListener(marker, 'click', function () {
                  var pws = this.id;
                  var url = 'https://api.wunderground.com/api/' + apiKey + '/history_' + date + '/q/pws:' + pws + '.json';
                  var content = '<a href="' + url + '">' + url + '</a>';
                  content += '<div id="svg-div"></div>';
                  arrayClean(calls);
                  getPwsHistory(processPwsHistory, pws, date, this);
                  infowindow.setContent(content);
                  infowindow.open(map, this);
                });        
                */
              }
            },
            error : function(jqXHR, status, error) {
              console.log(jqXHR);
              console.log(status);
              console.log(error);
              console.log("error");
            }
          });
          // end of ajax call
        }
        else {
          alert("You have reached the query limit per minute (10).");
        }
      }
      else {
        alert("You have reached the daily query limit (500).");
      }


    }
  }
  map.on('contextmenu', rightClickOnMap);

  /*
  google.maps.event.addListener(map, 'rightclick', function(event) {
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    arrayClean(calls);
    if (typeof apiKey != 'undefined' && apiKey.length > 0) {
      if (totalcalls < totalcallsLimit) {
        if (calls.length < callsLimit) {
          $.ajax({
            url : "http://api.wunderground.com/api/" + apiKey + "/geolookup/q/" + lat + "," + lng + ".json",
            dataType : "jsonp",
            success : function(parsed_json) {
              increaseCalls();
              var stations = parsed_json['location']['nearby_weather_stations'];
              // *** Loop through airports 
              for (i=0; i < stations['airport']['station'].length; i++) {
                station = stations['airport']['station'][i];

                // Only display aiports if they have icao.
                if (station['icao'].length > 0) {
                  var myLatlng = new google.maps.LatLng(station['lat'],station['lon']);
                  var marker = new google.maps.Marker({
                    position: myLatlng,
                    title: station['city'],
                    icon: './images/airport.png',
                    info: station['icao'],
                    map: map,
                  });

                  google.maps.event.addListener(marker, 'click', function () {
                    var airport = this.info;
                    var url = 'https://www.wunderground.com/history/airport/' + airport;
                    var content = '<a href="' + url + '">' + url + '</a>';
                    content += '<div id="svg-div"></div>';
                    arrayClean(calls);
                    getAirportHistory(processAirportHistory, airport, date, this);
                    infowindow.setContent(content);
                    infowindow.open(map, this);
                  });
                }
              }
              // *** Loop through personal weather stations
              for (i=0; i < stations['pws']['station'].length; i++) {
                station = stations['pws']['station'][i];
                var myLatlng = new google.maps.LatLng(station['lat'],station['lon']);
                var marker = new google.maps.Marker({
                  position: myLatlng,
                  title: station['id'] + ' ' + station['neighborhood'],
                  icon: './images/pws.png',
                  id: station['id'],
                  info: '<p>id: ' + station['id'] + '<BR>City: ' + station['city'] + '<BR>Neighborhood: ' + station['neighborhood'] + '</p>',
                  map: map,
                });
                google.maps.event.addListener(marker, 'click', function () {
                  var pws = this.id;
                  var url = 'https://api.wunderground.com/api/' + apiKey + '/history_' + date + '/q/pws:' + pws + '.json';
                  var content = '<a href="' + url + '">' + url + '</a>';
                  content += '<div id="svg-div"></div>';
                  arrayClean(calls);
                  getPwsHistory(processPwsHistory, pws, date, this);
                  infowindow.setContent(content);
                  infowindow.open(map, this);
                });        
              }
            },
            error : function(jqXHR, status, error) {
              console.log(jqXHR);
              console.log(status);
              console.log(error);
              console.log("error");
            }
          });
          // end of ajax call
        }
        else {
          alert("You have reached the query limit per minute (10).");
        }
      }
      else {
        alert("You have reached the daily query limit (500).");
      }


    }
    
  });

  */

  /* Cleans callsArray of the items older then 1 minute.
  */
  function arrayClean(callsArray) {
    var now = new Date().getTime();
    for (i = 0; i < callsArray.length; i++) {
      if (callsArray[i] < now - 60 * 1000) {
        callsArray.shift();
        arrayClean(callsArray);
      }
      else {
        break;
      }
    }
  }

  function increaseCalls() {
    calls.push(new Date().getTime()); 
    totalcalls += 1;
    document.getElementById("calls-value").innerText = calls.length;
    document.getElementById("totalcalls-value").innerText = totalcalls;
  }

  //Temporary array to store history results in timely order.
  var historyStore = new Array();

  function concatTempArray() {
    var result = {hum: [], tempm: [], wspdm: [], precipm: []};
    for (var i = 0; i < historyStore.length; i++) {
      if (typeof historyStore[i] != 'undefined' && historyStore[i] instanceof Object) {
        var newHumArray = historyStore[i].hum.concat(result.hum);
        var newTempmArray = historyStore[i].tempm.concat(result.tempm);
        var newWspdmArray = historyStore[i].wspdm.concat(result.wspdm);
        var newPrecipmArray = historyStore[i].precipm.concat(result.precipm);
        result.hum = newHumArray;
        result.tempm = newTempmArray;
        result.wspdm = newWspdmArray;
        result.precipm = newPrecipmArray;
      }
    }
    return result;
  }

  var settings = {
    top: 20,
    right: 80,
    bottom: 30,
    left: 50,
    chartid: "",
    yname: "",
    ylabel: "",
    classname: ""
  }
  
  function displayChart(data) {
    var setting = settings;
    
    if(typeof data.precipm != "undefined" && data.precipm.length > 2) {
      setting.chartid = "precipm-chart";
      setting.yname = "precipm";
      setting.ylabel = "Precipitation";
      setting.classname = "precipline";

      drawChart(data, setting);
    }

    if(typeof data.tempm != "undefined" && data.tempm.length > 2) {
      setting.chartid = "temp-chart";
      setting.yname = "tempm";
      setting.ylabel = "Temperature, ºC";
      setting.classname = "templine";

      drawChart(data, setting);
    }

    if(typeof data.hum != "undefined" && data.hum.length > 2) {
      setting.chartid = "hum-chart";
      setting.yname = "hum";
      setting.ylabel = "Humidity %";
      setting.classname = "humline";

      drawChart(data, setting);
    }

    if(typeof data.wspdm != "undefined" && data.wspdm.length > 2) {
      setting.chartid = "wspdm-chart";
      setting.yname = "wspdm";
      setting.ylabel = "Wind speed (km/h)";
      setting.classname = "wspdmline";

      drawChart(data, setting);

    }
  }

  /*
  The callback function of getAirportHistory().
  */
  function processAirportHistory(json, i) {
    var history = json.history;
    var history = validateHistory(history);
    historyStore[i] = history;
    var data = concatTempArray();
    displayChart(data);
  }

  function getAirportHistory(callback, airport, date, marker) {
    for (var i = 0; i < callsLimit - calls.length && totalcalls < totalcallsLimit; i++) {
      var url = "http://api.wunderground.com/api/" + apiKey + "/history_" + (date - i) + "/q/" + airport + ".json";
      $.ajax({
        i: i,
        url : url,
        dataType : "jsonp",
        success: function(response) {
            increaseCalls();
            callback(response, this.i);
          }
      });
    }
  }

  /*
  The callback function of getPwsHistory().
  */
  function processPwsHistory(json, i) {
    var history = json.history;
    var history = validateHistory(history);
    historyStore[i] = history;
    var data = concatTempArray();
    displayChart(data);
  }
  
  function getPwsHistory(callback, pws, date, marker) {
    for (var i = 0; i < callsLimit - calls.length && totalcalls < totalcallsLimit; i++) {
      var url = 'http://api.wunderground.com/api/' + apiKey + '/history_' + (date - i) + '/q/pws:' + pws + '.json';
      $.ajax({
        i: i,
        url : url,
        dataType : "jsonp",
        success: function(response) {
          increaseCalls();
          callback(response, this.i);
        }
      });
    }
  }

  /*
  This function removes incorrect data from the array, and returns te cleaned array.
  */
  function validateHistory(history) {
    var parseTime = d3.timeParse("%Y%m%d%H%M");
    var parseDate = d3.timeParse("%Y%m%d");
    var tempmArray = new Array();
    var humArray = new Array();
    var wspdmArray = new Array();
    var precipmArray = new Array();
    history.observations.forEach(function(item, index) {
      item.date2 = item.date.year + item.date.mon + item.date.mday + item.date.hour + item.date.min;
      item.date2 = parseTime(item.date2);
      if (item.wsdpm != "" && item.wspdm >= 0 && item.wspdm < 450) {
        wspdmArray.push({date: item.date2, wspdm: item.wspdm});
      }
      else {
        console.log("invalid wspdm");
        console.log(item.wspdm);
      }
      if (item.tempm != "" && item.tempm < 70 && item.tempm > -60) {
        tempmArray.push({date: item.date2, tempm: item.tempm});
      }
      else {
        console.log("invalid tempm");
        console.log(item.tempm);
      }
      if (item.hum != "" && item.hum > 0 && item.hum <= 100) {
        humArray.push({date: item.date2, hum: item.hum})
      }
      else {
        console.log("invalid hum");
        console.log(item.hum);
      }
    })
    
    if (history.dailysummary[0].precipm != "" && history.dailysummary[0].precipm >= 0 && history.dailysummary[0].precipm <= 2000) {
        history.dailysummary[0].date2 =  history.dailysummary[0].date.year + history.dailysummary[0].date.mon + history.dailysummary[0].date.mday;
        history.dailysummary[0].date2 = parseDate(history.dailysummary[0].date2);
        precipmArray.push({date: history.dailysummary[0].date2, precipm: history.dailysummary[0].precipm})
      }
      else {
        console.log("invalid precipm");
        console.log(history.dailysummary[0].precipm);
      }

    var history = {"tempm": tempmArray, "hum": humArray, "wspdm": wspdmArray, "precipm": precipmArray};
    return history
  }

  function drawChart(data, settings) {

    var margin = {top: settings.top, right: settings.right, bottom: settings.bottom, left: settings.left},
      width = 600 - margin.left - margin.right,
      height = 200 - margin.top - margin.bottom;
 
    var x = d3.scaleTime().range([0, width]);
    var y = d3.scaleLinear().range([height, 0]);
    d3.select('#svg-div').selectAll("#" + settings.chartid).remove();
    var svg = d3.select('#svg-div')
      .append('svg')
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("id", settings.chartid);

    var g = svg.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")"); 

    x.domain(d3.extent(data[settings.yname], function(d) { return d.date; }));
    y.domain(d3.extent(data[settings.yname], function(c) { return parseInt(c[settings.yname]); }));
    g.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x));

    g.append("g")
      .attr("class", "y axis")
      .call(d3.axisLeft(y))
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("fill", "#000")
      .text(settings.ylabel);

    var chartLine = d3.line()
      .curve(d3.curveBasis)
      .x(function(d) { 
        return x(d.date); 
      })
      .y(function(d) {
        return y(d[settings.yname]);
    });
    
    g.append("path")
      .datum(data[settings.yname])
      .attr("class", settings.classname)
      .attr("d", chartLine);
  }

});


// sample query for airport history
// http://api.wunderground.com/api/a2b765a30acf142a/history_20101018/q/LHSM.json
// sample query for wps history
// http://api.wunderground.com/api/a2b765a30acf142a/history_20101018/q/pws:KCASANFR14.json

/*
Chrome does not save cookies on localhost, I have to implement it later.
 function setCookie() {
    var key = document.getElementById("api-key").value;
	document.cookie = key;
  }
  document.getElementById("api-form").addEventListener("change", setCookie);
  
  function getCookie(){
    alert(document.cookie);
  }
  getCookie();  
  */

// Removes elements from the input array that are older then one minute.  
